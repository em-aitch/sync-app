import { Component, Injectable } from '@angular/core';
import { createStateSyncMiddleware, initStateWithPrevTab, withReduxStateSync } from 'redux-state-sync';
import { combineReducers, Action, createStore, applyMiddleware } from 'redux';
import { NgRedux, select } from '@angular-redux/store';
import { Observable } from "rxjs";

export interface LoginToggle {
  isLoggedIn : boolean
}

export interface LoginToggleAction extends Action {
  loginToggle: LoginToggle;
}

@Injectable()
export class LoginActions {
  static TOGGLE_LOGIN = 'TOGGLE_LOGIN';

  toggleLogin(loginToggle: LoginToggle): LoginToggleAction {
    return { 
        type: LoginActions.TOGGLE_LOGIN, 
        loginToggle 
    };
  }
}

export interface ILoginState {
  readonly isLoggedIn: boolean;
}

export interface IApplicationState {
  login: ILoginState;
}

export const INITIAL_STATE : IApplicationState = {
   login : { isLoggedIn: false } 
}

export function loginReducer(oldState: ILoginState = { isLoggedIn : false } as any, action: Action) : ILoginState {
  switch (action.type) {
      case LoginActions.TOGGLE_LOGIN: {
          console.log('in reducer');
          const toggleAction = action as LoginToggleAction;
          return {
              ...oldState,
              isLoggedIn: toggleAction.loginToggle.isLoggedIn
          } as ILoginState;       
      }
      default: {
        return oldState;
    }
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  statusTxt = 'logged out';
  subscription;
  loginStatus = false;
  buttonTxt = 'Login';

  @select((state: IApplicationState) => state.login.isLoggedIn) isLoggedIn: Observable<boolean>;

  constructor(private ngRedux: NgRedux<IApplicationState>, 
    private actions : LoginActions) {
    
    const appReducer = combineReducers<IApplicationState>({
      login: loginReducer
    })

    const rootReducer = withReduxStateSync(appReducer);
    const store = createStore(rootReducer, applyMiddleware(createStateSyncMiddleware({ broadcastChannelOption: { type: 'localstorage' } })));
    
    initStateWithPrevTab(store);
    
    ngRedux.provideStore(store);

    this.subscription = this.ngRedux.select<ILoginState>('login')
      .subscribe(newState => {
        console.log('new login state = ' + newState.isLoggedIn);
        this.loginStatus = newState.isLoggedIn;
        if (newState.isLoggedIn) {
          this.statusTxt = 'logged in!';
          this.buttonTxt = 'Logout';
        }
        else { 
          this.statusTxt = 'logged out!';
          this.buttonTxt = 'Login';
        }
        console.log('statusTxt = ' + this.statusTxt);
    });
  }

  toggleLogin(): void {
    this.ngRedux.dispatch(this.actions.toggleLogin({ isLoggedIn: !this.loginStatus }));
  }
}
